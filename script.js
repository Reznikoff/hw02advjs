
const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
  ];

    const root = document.getElementById("root")

    const ul = document.createElement("ul");
    
    
    books.forEach((book,i,arr) => {
      try{
        if (!book.author||!book.name||!book.price){
          
        throw new Error (`немає таких значень  в обєкті під індексом ${i}:${book.author?"":" author "}${book.name?"":"name "}${book.price?"":" price"} `)  
        } 
  
        const li = document.createElement('li');
    
        li.textContent = `${book.author}: ${book.name} - ${book.price} грн`;
      
        ul.appendChild(li);
  
      }
      catch(e){
        console.log(e.message)
      }
      });
    root.appendChild(ul);